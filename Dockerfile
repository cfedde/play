FROM centos
MAINTAINER christopher_fedde@cable.comcast.com
RUN yum install -y \
    bzip2 \
    curl \
    expat \
    expat-devel \
    gcc \
    git \
    hostname \
    libxslt-devel \
    locate \
    make \
    openssl \
    openssl-devel \
    tar \
    vim \
    wget \
    zip

RUN wget http://www.cpan.org/src/5.0/perl-5.22.0.tar.gz; \
    tar -xvf perl-5.22.0.tar.gz; \
    cd perl-5.22.0; \
    ./Configure -des -Dprefix=$HOME/localperl; \
    make install

RUN echo 'PATH=$HOME/localperl/bin:$PATH' >> $HOME/.bashrc
RUN bash -c '. ~/.bashrc ;\
            wget https://cpanmin.us -O - | perl - App::cpanminus ;\
            cpanm Task::Kensho -qn'

RUN bash -c '. ~/.bashrc ;\
    cpanm -n http://bcvtools.comcast.net/pinto/authors/id/C/CF/CFEDDE/BCV-AS-0.19.tar.gz ;\
    cpanm -n http://bcvtools.comcast.net/pinto/authors/id/C/CF/CFEDDE/BCV-OCIWrapper-0.35.tar.gz ;'

